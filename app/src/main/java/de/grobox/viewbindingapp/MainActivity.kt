package de.grobox.viewbindingapp

import android.app.Activity
import android.os.Bundle
import de.grobox.viewbindingapp.databinding.ActivityMainBinding

class MainActivity : Activity() {

    // https://developer.android.com/topic/libraries/view-binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val ui = ActivityMainBinding.inflate(layoutInflater)
        setContentView(ui.root)

        ui.textView.text = "VIEW BINDING WORKS!!!"
    }

}